using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public int health = 3;
    public float speed;
    public float jumpForce;

    public GameObject Magic;
    public Transform firePoint;

    private bool isJumping;
    private bool doubleJump;
    private bool isFire;

    private Rigidbody2D rig;
    private Animator anim;

    private float movement;

    // o Awake vem antes do start 
    void Start()
    {

        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        GameControler.instance.UpdateLives(health);
    }

    // Update is called once per frame
    void Update()
    {
       Move();
       Jump();
       ChargeMagic(); 
    }

    void Move ()
    {
        movement = Input.GetAxis("Horizontal");

        //adiciona velocidade no corpo do personagem em ambos os eixos x e y a partir do rigdbody
        rig.velocity = new Vector2(movement * speed, rig.velocity.y);

        //andando pra a direita
        if(movement > 0)
        {
            anim.SetInteger("transition", 1);

            transform.eulerAngles = new Vector3(0, 0, 0);
        }

        //andando pra a direita
        if(movement < 0)
        {
            anim.SetInteger("transition", 1);

            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if(movement == 0 && !isFire)
        {
            anim.SetInteger("transition", 0);
        }
    }

    void Jump()
    {
        if(Input.GetButtonDown("Jump"))
        {
            if(!isJumping)
            {
                rig.AddForce(new Vector2(0f, jumpForce * 2), ForceMode2D.Impulse);
                doubleJump = true;
            }
            else
            {
                if(doubleJump)
                {
                    rig.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
                    doubleJump = false;
                }
            }
        }
    }

    void ChargeMagic()
    {
       StartCoroutine("Fire");
    }

    IEnumerator Fire()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            anim.SetInteger("transition", 2);
            isFire = true;
            GameObject magic = Instantiate(Magic, firePoint.position, firePoint.rotation);

            if(transform.rotation.y == 0)
            {
                magic.GetComponent<Magic>(). isRight = true;
            }

            if(transform.rotation.y == 180)
            {
                magic.GetComponent<Magic>(). isRight = false;
            }

            yield return new WaitForSeconds(0.4f);
            anim.SetInteger("transition", 0);
        }
    }

    public void Damage(int damage)
    {
        health -= damage;
        GameControler.instance.UpdateLives(health);
        anim.SetTrigger("hit");

        if(transform.rotation.y == 0)
        {
            transform.position += new Vector3(-1f, 0, 0);
        }

        if(transform.rotation.y == 180)
        {
            transform.position += new Vector3(1f, 0, 0);    
        }

        if(health <= 0)
        {
            //chamar game over
            GameControler.instance.GameOver();
        }
    }

    public void IncreaseLife(int value)
    {
        health += value;
        GameControler.instance.UpdateLives(health);
    }

    //supostos limitadores de pulo, o pulo fica ilimitado por algum motivo entao isso virou uma mecanica do jogo onde vc pode voar :)
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.layer == 8)
        {
            isJumping = false;
        }
    }
    void OnCollisionExit2D(Collision2D coll)
    {
        if(coll.gameObject.layer == 8)
        {
            isJumping = true;
        }
    }
}
